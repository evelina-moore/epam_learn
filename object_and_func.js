//Create a car object, add a color property to it with the value 'black':
const car = {
  color: 'black'
};

//Change the color property of the car object to 'green':
car.color = 'green';

//Add the power property to the car object, which is a function displaying engine power:
car.power = function() {
  console.log('Engine power is displayed.');
};

//Pears and apples are accepted to the warehouse, write a function that returns the result of adding their quantities:
function addFruits(pears, apples) {
  return pears + apples;
}

//Your name is saved in the payment terminal, write a function to define the name in the terminal:
function greetUser(name) {
  if (name) {
    console.log('Hello ' + name);
  } else {
    console.log('There is no name saved.');
  }
}

//Write a function for calculating the type of argument and outputting it to the console:
function logType(arg) {
  console.log('Type of argument: ' + typeof arg);
}

//Write a function that determines whether a number is prime or not:
function isPrime(number) {
  if (number <= 1) return false;
  for (let i = 2; i <= Math.sqrt(number); i++) {
    if (number % i === 0) return false;
  }
  return true;
}